import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.text.SimpleDateFormat
import java.util.*

plugins {
	id("org.springframework.boot") version "2.7.10"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("com.google.cloud.tools.jib") version "3.0.0"
	kotlin("jvm") version "1.8.20"
	kotlin("plugin.spring") version "1.8.20"
}

group = "nl.tno.ids"
version = "4.3.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
	maven {
		url = uri("https://nexus.dataspac.es/repository/tsg-maven/")
	}
	maven {
		url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
	}
}

val springVersion = "2.7.10"
val jacksonVersion = "2.12.+"
val tnoIdsVersion = "4.6.1-SNAPSHOT"
val rdf4jVersion = "4.2.3"
val jsonVersion = "20230227"

dependencies {
	implementation("org.jetbrains.kotlin:kotlin-stdlib")

	implementation("nl.tno.ids:base-data-app:${tnoIdsVersion}")
	implementation("org.eclipse.rdf4j:rdf4j-sail-memory:${rdf4jVersion}")
	implementation("org.eclipse.rdf4j:rdf4j-repository-sail:${rdf4jVersion}")
	implementation("org.eclipse.rdf4j:rdf4j-rio-jsonld:${rdf4jVersion}")
	implementation("org.eclipse.rdf4j:rdf4j-rio-turtle:${rdf4jVersion}")
	implementation("org.eclipse.rdf4j:rdf4j-repository-sparql:${rdf4jVersion}")
	implementation("org.eclipse.rdf4j:rdf4j-queryresultio-text:${rdf4jVersion}")

	implementation("org.json:json:${jsonVersion}")

	implementation("org.springframework.boot:spring-boot-starter-security:${springVersion}")

	testImplementation("org.springframework.boot:spring-boot-starter-test:$springVersion")
	testImplementation("org.apache.jena:jena-fuseki-main:4.7.0")
	testImplementation("io.micrometer:micrometer-core:1.10.6")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

/**
 * Use Google Container Tools / jib to build and publish a Docker image.
 * This task is ran using Gitlab Runners, defined in .gitlab-ci.yml
 * Publish Docker image to repository
 */
jib {
	to {
		val imageName = System.getenv().getOrDefault("IMAGE_NAME", "docker.nexus.dataspac.es/broker/data-app")
		val imageTag = System.getenv().getOrDefault("IMAGE_TAG", "local").replace('/', '-')
		val imageTags = mutableSetOf(imageTag)
		if (System.getenv().containsKey("CI")) {
			imageTags += "${imageTag}-${SimpleDateFormat("yyyyMMddHHmm").format(Date())}"
		}
		image = imageName
		tags = imageTags
	}

	container {
		jvmFlags = listOf("-Xms512m", "-Xmx512m")
		ports = listOf("8080/tcp")
		creationTime = "USE_CURRENT_TIMESTAMP"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
