# Broker Data App

The Broker Data App provides the business logic for IDS Metadata Broker responsibilities that can be combined with a [TSG Core Container](https://gitlab.com/tno-tsg/core-container) to create a TSG Metadata Broker.

The Broker Data App is built using the [TSG Base Data App library](https://gitlab.com/tno-tsg/data-apps/base-data-app), and provides the logic to handle `QueryMessage`s, `ConnectorNotificationMessage`s, `ParticipantNotificationMessage`s, and an optional non-standardized `ArtifactRequestMessage` handler for Broker federation support.

The data app connects to a SPARQL service, tested primarily with Apache Fuseki but other SPARQL-enabled servers should also work, to store and query the information provided by the Metadata Broker.

## Deployment

The primary way of deploying the Broker Data App is via the [TSG Connector Helm Chart](https://gitlab.com/tno-tsg/helm-charts/connector), by providing roughly the following configuration in the `values.yaml` file:
```yaml

ids:
  # ...
  routes:
    ingress:
      http:
        - endpoint: infrastructure
          dataApp: http://{{ template "tsg-connector.fullname" $ }}-broker-data-app-http:8080/router
        - endpoint: participants
          dataApp: http://{{ template "tsg-connector.fullname" $ }}-broker-data-app-http:8080/participants
  # ...

containers:
  # ...
  - type: data-app
    image: docker.nexus.dataspac.es/data-apps/broker-data-app:4.2.0
    name: broker-data-app
    config:
      connectorSparqlUrl: http://fuseki:3030/connectorData
      participantSparqlUrl: http://fuseki:3030/participantData
      users:
        admin: $2a$12$RZ/aIqtt3a90rc1JngLLHusRnI9sG9x2jieFcZhmL.4ZNh9vvpxCq
  # ...
```

## Structure

The Broker Data App is split up into several packages:
- `nl.tsg.broker`: Root package with the main Application class and the Spring Security configuration.
- `nl.tsg.broker.util`: Utility classes, including the `RepositoryProviders` for both the Connectors as Participants.
- `nl.tsg.broker.connectordescription`: Message handler and persister for `ConnectorNotificationMessage`s.
- `nl.tsg.broker.participantdescription`: Message handler and persister for `ParticipantNotificationMessage`s.
- `nl.tsg.broker.query`: Message handler for `QueryMessage`s.
- `nl.tsg.broker.config`: Spring Configuration properties used in the data app.
- `nl.tsg.broker.ui`: Spring Rest Controllers for UI & Administration APIs.


## Build

The broker extension is built via Gradle, via the Gradle wrapper, usually directly into a Docker image by using Jib.

To locally build the project (and execute tests) execute:
```bash
./gradlew build
```
Or to only build and not run tests:
```bash
./gradlew assemble
```

To just execute the tests, execute:
```bash
./gradlew test
```

> _Note_: The tests use the Flapdoodle MongoDB dependency that downloads the MongoDB server for your machine as well as Wiremock to provide a mock service for HTTP requests. Both will run on a specific port that must be available.

To build the Docker image via the Google Jib extension, execute:
```bash
./gradlew jib
```
This automatically builds the Docker image `docker.nexus.dataspac.es/broker/data-app:local` and pushes it to the Docker registry.
By setting the `IMAGE_NAME` and/or `IMAGE_TAG` environment variables the Docker image can be modified.

To prevent pushing the Docker image being pushed to the registry, execute either:
```bash
./gradlew jibDockerBuild
```
To build the image via a Docker daemon (requires a Docker daemon to be available), or:
```bash
./gradlew jibBuildTar
```
To build the image into a tarball.

## Configuration

Configuration of the Broker Data App uses Spring, to allow setting configuration via properties/YAML files but also allowing overrides via environment variables.

The following properties can be set:

| Key | Type | Default | Description |
| --- | --- | --- | --- |
| `connectorSparqlUrl` | URL | - | SPARQL URL that can be used to query and modify data |
| `participantSparqlUrl` | URL | - | SPARQL URL that can be used to query and modify data |
| `users` | `{ [username: string]: string }` | `{}` | Map containing administrative users, where the key is the username and the value is the BCrypt encoded password |
| `initConnectors` | String[] | `[]` | List of JSON-LD documents of initial Connector Self-Descriptions |
| `initParticipants` | String[] | `[]` | List of JSON-LD documents of initial Participant Self-Descriptions |
| `validation` | String[] | `[]` | List of optional validation servers that can execute additional checks to see whether an update is allowed |
| `federationType` | String[] | `[]` | Type of federation with the federated brokers, may contain `PUBLISH_TIME`, `QUERY_TIME`, `CONNECTOR_CONTROLLED` |
| `federatedBrokers[].accessUrl` | String | - | Federated Broker access URL |
| `federatedBrokers[].connectorId` | String | - | Federated Broker Connector ID |
| `federatedBrokers[].tags` | String[] | `[]` | Locally set tags for the Federated Broker |


### Federated Brokers

This Broker Data App supports Federation of Brokers, allowing to link Metadata Brokers in different data spaces to work together.

Three different ways of federation are supported:
- `PUBLISH_TIME`: Each self-description submitted to one of the brokers is shared with the connected Federated Brokers.
- `QUERY_TIME`: Each query request submitted to one of the brokers is shared with the connected Federated Brokers.
- `CONNECTOR_CONTROLLER`: The broker does not actively share any data with connected Federated Brokers, but only provides the list of Federated Brokers to the connectors when requested via `ArtifactRequestMessage`s. The connectors can then decide whether they want to share their self-description with other brokers, or query other brokers.
