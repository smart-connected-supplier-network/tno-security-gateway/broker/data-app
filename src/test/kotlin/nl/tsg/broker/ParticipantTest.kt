package nl.tsg.broker

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.net.URI

//@Disabled
class ParticipantTest {
  @Test
  fun test() {
    val connectorUnavailableMessage = ConnectorUnavailableMessageBuilder()
      ._issued_(DateUtil.now())
      ._issuerConnector_(URI.create("https://ids.tno.nl/connectors/Broker"))
      ._senderAgent_(URI.create("senderAgent"))
      ._recipientAgent_(Util.asList(URI.create("broker")))
      ._modelVersion_("2.0.0")
      ._affectedConnector_(URI.create("https://testconnector/"))
      .build()
    val multiPartMessage = MultiPartMessage.Builder().setHeader(connectorUnavailableMessage).build()
    println(multiPartMessage.toString())
  }

  @Test
  fun testParticipant() {
    val participant = ParticipantBuilder(URI.create("https://ids.tno.nl/participants/TNO"))
      ._title_(
        arrayListOf(
          TypedLiteral("Nederlandse Organisatie voor toegepast-natuurwetenschappelijk onderzoek", "nl"),
          TypedLiteral("Netherlands Organisation for Applied Scientific Research", "en")
        )
      )
      ._memberParticipant_(
        arrayListOf(
          ParticipantBuilder(URI.create("https://ids.tno.nl/participants/TNO/MCS"))
            ._title_(arrayListOf(TypedLiteral("Monitoring & Control Services", "en")))
            ._legalForm_("54M6")
            .build(),
          ParticipantBuilder(URI.create("https://ids.tno.nl/participants/TNO/DS"))
            ._title_(arrayListOf(TypedLiteral("Data Science", "en")))
            ._legalForm_("54M6")
            .build()
        )
      )
      ._memberPerson_(
        arrayListOf(
          PersonBuilder()
            ._givenName_("Maarten")
            ._familyName_("Kollenstart")
            ._emailAddress_(arrayListOf("maarten.kollenstart@tno.nl"))
            ._phoneNumber_(arrayListOf("0611428437"))
            .build()
        )
      )
      ._corporateHomepage_(URI.create("https://tno.nl"))
      ._corporateEmailAddress_(arrayListOf("contact@tno.nl"))
      ._primarySite_(SiteBuilder()._siteAddress_("Anna van Buerenplein 1, 2595 DA Den Haag").build())
      ._legalForm_("54M6")
      ._description_(TypedLiteral("logo", "LOGO"))
      .build()
    val participantJson = SerializationHelper.getInstance().toJsonLD(participant)
    SerializationHelper.getInstance().fromJsonLD(participantJson, Participant::class.java)
  }

  @Test
  fun serializeAndDeserializeConnector() {
    val trustedConnector = TrustedConnectorBuilder()
      ._description_(arrayListOf(TypedLiteral("test Connector to verify the IM", "en")))
      ._maintainer_(URI.create("urn:scsn:ids:participants:TNO"))
      ._curator_(URI.create("urn:scsn:ids:participants:TNO"))
      ._securityProfile_(SecurityProfile.BASE_SECURITY_PROFILE)
      ._outboundModelVersion_("4.0.1")
      ._resourceCatalog_(arrayListOf(
        ResourceCatalogBuilder()
          ._offeredResource_(arrayListOf(ResourceBuilder().build()))
          .build()
      ))
      ._hasDefaultEndpoint_(ConnectorEndpointBuilder()
        ._accessURL_(URI("http://localhost"))
        .build())
      .build()

    val connectorJson = SerializationHelper.getInstance().toJsonLD(trustedConnector)
    SerializationHelper.getInstance().fromJsonLD(connectorJson, Connector::class.java)
  }
}