package nl.tsg.broker.federation

import de.fraunhofer.iais.eis.ArtifactRequestMessage
import de.fraunhofer.iais.eis.ArtifactResponseMessageBuilder
import de.fraunhofer.iais.eis.DynamicAttributeTokenBuilder
import de.fraunhofer.iais.eis.TokenFormat
import nl.tno.ids.base.StringMessageHandler
import nl.tno.ids.base.configuration.IdsConfig
import nl.tsg.broker.config.BrokerConfig
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.DateUtil
import org.json.JSONArray
import org.json.JSONObject
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.net.URI

@Component
class FederationArtifactHandler(
  private val brokerConfig: BrokerConfig,
  private val idsConfig: IdsConfig
): StringMessageHandler<ArtifactRequestMessage> {
  override fun handle(header: ArtifactRequestMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {

    val artifactResponseMessage = ArtifactResponseMessageBuilder()
      ._modelVersion_(idsConfig.modelVersion)
      ._issued_(DateUtil.now())
      ._securityToken_(
        DynamicAttributeTokenBuilder()
          ._tokenFormat_(TokenFormat.JWT)
          ._tokenValue_("DUMMY")
          .build()
      )
      ._correlationMessage_(header.id)
      ._senderAgent_(URI(idsConfig.participantId))
      ._issuerConnector_(URI.create(idsConfig.connectorId))
      ._recipientConnector_(arrayListOf(header.issuerConnector))
      ._recipientAgent_(arrayListOf(header.senderAgent))
      .build()

    val multiPartMessage = MultiPartMessage.Builder()
      .setHeader(artifactResponseMessage)
      .setPayload(JSONArray(
        brokerConfig.federatedBrokers.map {
          JSONObject()
            .put("connectorId", it.connectorId)
            .put("accessUrl", it.accessUrl)
            .put("tags", it.tags)
        }
      ).toString())
      .build()
    val response = MultiPart.toString(multiPartMessage, false)
    val mediaType: String = multiPartMessage.httpHeaders["Content-Type"] ?: ""
    return ResponseEntity.ok().contentType(MediaType.parseMediaType(mediaType)).body(response)
  }

}