package nl.tsg.broker.config

/*
A federated broker is a broker that is connected to the current broker.
When connectors are published to the broker, they can be published to federated brokers as well.
 */
data class FederatedBroker(
  val accessUrl: String,
  val connectorId: String,
  /* tags can be used to filter federation to certain types of brokers */
  val tags: List<String> = emptyList()
)
