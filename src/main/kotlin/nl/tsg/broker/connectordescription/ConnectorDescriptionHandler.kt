package nl.tsg.broker.connectordescription

import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.base.StringMessageHandler
import nl.tno.ids.base.configuration.CoreContainerConfig
import nl.tno.ids.base.configuration.IdsConfig
import nl.tsg.broker.config.BrokerConfig
import nl.tsg.broker.config.FederationType
import org.apache.http.entity.ContentType
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.io.IOException
import java.net.URI
import java.util.function.Consumer
import javax.annotation.PostConstruct

@Component
class ConnectorDescriptionHandler(
  private val brokerConfig: BrokerConfig,
  private val coreContainerConfig: CoreContainerConfig,
  private val idsConfig: IdsConfig,
  private val connectorDescriptionPersister: ConnectorDescriptionPersister,
  private val responseBuilder: ResponseBuilder,
  private val httpHelper: HttpHelper
) : StringMessageHandler<ConnectorNotificationMessage> {

  @PostConstruct
  fun postConstruct() {
    brokerConfig.initConnectors.forEach(Consumer { connector: String ->
      try {
        connectorDescriptionPersister.ingestSelfDescription(connector)
      } catch (e: IOException) {
        e.printStackTrace()
      }
    })
  }

  private fun validate(url: String, incoming: ConnectorNotificationMessage, payload: String): ResponseEntity<*>? {
    try {
      val response = httpHelper.forwardSynchronous(url, "", incoming, payload, null, ContentType.APPLICATION_JSON)
      if (response.first in 200..400) {
        return null
      } else {
        ResponseEntity.status(response.first).body(response.second.toString())
      }
    } catch (e: IOException) {
      e.printStackTrace()
    }
    return responseBuilder.createRejectionResponse(incoming, RejectionReason.INTERNAL_RECIPIENT_ERROR, HttpStatus.INTERNAL_SERVER_ERROR)
  }

  override fun handle(header: ConnectorNotificationMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {
    var response: ResponseEntity<*>
    try {
      when (header) {
        is ConnectorUnavailableMessage -> {
          connectorDescriptionPersister.removeSelfDescription(header.getIssuerConnector())
          response = responseBuilder.createMessageProcessedResponse(header)
        }
        is ConnectorUpdateMessage -> {
          for (validationUrl in brokerConfig.validation) {
            val result: ResponseEntity<*>? = validate(validationUrl, header, payload ?: "")
            if (result != null) {
              LOG.warn("Validation failed: {}, {}, {}", validationUrl, header.getIssuerConnector(), payload)
            }
          }
          connectorDescriptionPersister.replaceSelfDescription(payload ?: "")
          response = responseBuilder.createMessageProcessedResponse(header)
        }
        else -> {
          // Other types of messages are not supported
          response = responseBuilder.createRejectionResponse(header, RejectionReason.MESSAGE_TYPE_NOT_SUPPORTED, HttpStatus.NOT_FOUND)
        }
      }
    } catch (e: IOException) {
      LOG.error("IOException in processing Connector Description. ", e)
      response = responseBuilder.createRejectionResponse(header, RejectionReason.INTERNAL_RECIPIENT_ERROR, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    // Federate the Connector message to other brokers in the federation
    if (brokerConfig.federationType.contains(FederationType.PUBLISH_TIME)) {
      LOG.info("Publish-time broker federation configured, federating {}", header::class.java)
      for (federatedBroker in brokerConfig.federatedBrokers) {
        LOG.info("Federating to dataspace {}, endpoint: {}", federatedBroker.connectorId, federatedBroker.accessUrl)
        when (header) {
          is ConnectorUpdateMessageImpl -> {
            header.issuerConnector = URI(idsConfig.connectorId)
            header.senderAgent = URI(idsConfig.participantId)
          }
          is ConnectorUnavailableMessageImpl -> {
            header.issuerConnector = URI(idsConfig.connectorId)
            header.senderAgent = URI(idsConfig.participantId)
          }
          is ConnectorCertificateGrantedMessageImpl -> {
            header.issuerConnector = URI(idsConfig.connectorId)
            header.senderAgent = URI(idsConfig.participantId)
          }
          is ConnectorCertificateRevokedMessageImpl -> {
            header.issuerConnector = URI(idsConfig.connectorId)
            header.senderAgent = URI(idsConfig.participantId)
          }
        }
        val federateResponse = httpHelper.forwardSynchronous(coreContainerConfig.httpsForwardEndpoint, federatedBroker.accessUrl, header, payload, null, ContentType.APPLICATION_JSON)
        if (federateResponse.first >= 400) {
          LOG.warn("Error (Status code {}) while federating message to dataspace {}: {}", federateResponse.first, federatedBroker.connectorId, federateResponse.second)
        }
      }
    }
    return response
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(ConnectorDescriptionHandler::class.java)
  }
}
