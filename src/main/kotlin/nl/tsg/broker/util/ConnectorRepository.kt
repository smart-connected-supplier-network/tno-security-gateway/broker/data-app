package nl.tsg.broker.util

import de.fraunhofer.iais.eis.Connector
import nl.tsg.broker.config.BrokerConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import javax.annotation.PostConstruct


@Component
class ConnectorRepository(private val brokerConfig: BrokerConfig, httpClient: CloseableHttpClient)
  : RepositoryProvider(httpClient) {
  private data class CacheEntry(
    val invalidAt: Long,
    val connectors: List<Connector>,
  )
  private val cache = ConcurrentHashMap<String, CacheEntry>()

  fun refreshCache(oldConnectors: List<Connector>? = null, compact: Boolean = true): CompletableFuture<List<Connector>> {
    val newInvalidAt = System.currentTimeMillis()+brokerConfig.uiCacheInterval
    cache["connectors${if (compact) "Compact" else "Full"}"] = CacheEntry(newInvalidAt, oldConnectors ?: cache["connectors${if (compact) "Compact" else "Full"}"]?.connectors ?: emptyList())
    return CompletableFuture.supplyAsync {
      val refresh = CacheEntry(newInvalidAt, queryConnectors())
      cache["connectors${if (compact) "Compact" else "Full"}"] = refresh
      refresh.connectors
    }
  }

  @PostConstruct
  private fun postConstruct() {
    sparqlUrl = brokerConfig.connectorSparqlUrl
    LOG.info("Setting SPARQL repository to be used: '$sparqlUrl'")
    for (i in 0..9) {
      try {
        repository = SPARQLRepository(sparqlUrl)
        repository!!.init()
        return
      } catch (e: Exception) {
        LOG.warn("Cannot connect to SPARQL Repository: {}", sparqlUrl)
      }
      try {
        Thread.sleep((10 * 1000).toLong())
      } catch (e: InterruptedException) {
        e.printStackTrace()
      }
    }
  }

  private fun queryConnectors(compact: Boolean = false) : List<Connector> {
    val query = if (compact) {
      """
      PREFIX ids: <https://w3id.org/idsa/core/>
      DESCRIBE * WHERE {
        GRAPH ?idsid {
          ?idsid ids:curator ?curator.
          ?idsid ids:maintainer ?maintainer.
          ?idsid ids:title ?title.
          ?idsid ids:description ?description.
          ?idsid ids:resourceCatalog ?rc.
          ?rc ids:offeredResource ?or.
          ?or ids:sovereign ?sovereign.
          ?or ids:title ?at.
          ?or ids:resourceEndpoint/ids:path ?path.
        }
      }
    """.trimIndent()
    } else {
      """
      PREFIX ids: <https://w3id.org/idsa/core/>
      DESCRIBE * WHERE {
        GRAPH ?g {
          ?s ?o ?p.
        }
      }
    """.trimIndent()
    }
    return query(query, Connector::class.java)
  }

  fun getAllConnectors(cacheEnabled: Boolean = true, compact: Boolean = false) : List<Connector> {
    return if (!cacheEnabled) {
      queryConnectors()
    } else {
      val cached = cache["connectors${if (compact) "Compact" else "Full"}"]
      if (cached == null || cached.invalidAt < System.currentTimeMillis()) {
        val refreshFuture = refreshCache(cached?.connectors ?: emptyList(), compact)
        return refreshFuture
          .completeOnTimeout(cached?.connectors ?: emptyList(), brokerConfig.maxUiTimeoutSeconds, TimeUnit.SECONDS)
          .get()
      } else {
        return cached.connectors
      }
    }
  }
}