package nl.tsg.broker.participantdescription

import nl.tsg.broker.util.ParticipantRepository
import nl.tsg.broker.util.RdfConverter
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class ParticipantDescriptionPersister (
  private val repository: ParticipantRepository
){
  @Autowired
  private val rdfConverter: RdfConverter? = null
  @Throws(IOException::class)
  fun ingestParticipantDescription(participantDescription: String) {
    LOG.info("Ingesting participant description")
    val result = rdfConverter!!.toRdf(participantDescription)
    repository.replaceStatements(result.namedGraph, result.model)
  }

  fun removeParticipantDescription(participant: String) {
    LOG.info("Removing participant description")
    val context = SimpleValueFactory.getInstance().createIRI(participant)
    repository.replaceStatements(context)
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(ParticipantDescriptionPersister::class.java)
  }
}