package nl.tsg.broker.ui

import nl.tno.ids.common.serialization.SerializationHelper
import nl.tsg.broker.config.BrokerConfig
import nl.tsg.broker.util.ConnectorRepository
import nl.tsg.broker.util.ParticipantRepository
import org.json.JSONObject
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api"])
class UserInterfaceController(
  private val connectorRepository: ConnectorRepository,
  private val participantRepository: ParticipantRepository,
  private val brokerConfig: BrokerConfig
) {
  @RequestMapping(value = ["/connectors"], method = [RequestMethod.GET])
  fun getConnectors(): String {
    return connectorRepository.getAllConnectors(cacheEnabled = true, compact = true).map { c ->
      JSONObject()
          .put("idsid", c.id.toString())
          .put("curator", c.curator.toString())
          .put("maintainer", c.maintainer.toString())
          .put("titles", c.title?.map {
            JSONObject()
                .put("value", it.value)
                .put("lang", it.language)
          })
          .put("descriptions", c.description?.map {
            JSONObject()
                .put("value", it.value)
                .put("lang", it.language)
          })
          .put("agents", c.resourceCatalog?.flatMap {
            it.offeredResource.map {
              JSONObject()
                  .put("id", it.sovereign?.toString())
                  .put("title", it.title?.firstOrNull()?.value)
                  .put("versions", it.resourceEndpoint?.map { it.path.split("/").last() })
            }
          })
    }.toString()
  }
  @RequestMapping(value = ["/connectors/full"], method = [RequestMethod.GET])
  fun getConnectorsFull(): String {
    if (brokerConfig.disableUiJsonLd) {
      return "[]"
    }
    return connectorRepository.getAllConnectors(cacheEnabled = true, compact = false).map { c ->
      JSONObject(SerializationHelper.getInstance().toJsonLD(c))
    }.toString()
  }

  @RequestMapping(value = ["/participants"], method = [RequestMethod.GET])
  fun getParticipants(): String {
    return participantRepository.getAllParticipants().map { p ->
      JSONObject()
          .put("idsid", p.id.toString())
          .put("contact", p.memberPerson?.firstOrNull()?.let { "${it.givenName} ${it.familyName}" })
          .put("titles", p.title?.map {
            JSONObject()
                .put("value", it.value)
                .put("lang", it.language)
          })
          .put("descriptions", p.description?.filter { it.language != "LOGO" }?.map {
            JSONObject()
                .put("value", it.value)
                .put("lang", it.language)
          }).apply {
            p.description?.firstOrNull { it.language == "LOGO" }?.let {
              put("logo", it.value)
            }
          }
    }.toString()
  }


  @RequestMapping(value = ["/participants/full"], method = [RequestMethod.GET])
  fun getParticipantsFull(): String {
    if (brokerConfig.disableUiJsonLd) {
      return "[]"
    }
    return participantRepository.getAllParticipants().map { p ->
      JSONObject(SerializationHelper.getInstance().toJsonLD(p))
    }.toString()
  }
}